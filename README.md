# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* A notification system is a method of facilitating the one-way dissemination or broadcast of messages to one or many groups of people, alerting them to a pending or existing situation, statistics data collection, behavior control in real time.


### How do I get set up? ###

* git clone git@bitbucket.org:dardanll/notification_system.git
* npm install
* node seed.js (which creates admin user with username: 'admin' and password: 'admin'))
* Start -> node bin/wwww