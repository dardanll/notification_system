var Activity = require('../../models/activity');
var Account = require('../../models/account');

module.exports = (io) => {
  return function (data) {
    Account.findOne({_id: data.user_id}).exec (function(err, user){
      if(err) console.log(err);

      var activity = new Activity({type: data.type, message: data.msg, author: user})
      activity.author = user;

      activity.save(function(err) {
        user.activities.push(activity);
        user.save(function(err) {
          if(err)
            console.log(err);
        });
      });
    });

    io.emit('chat message', {
      username: data.username,
      msg: data.msg,
      admin: data.admin
    });
  }
}