var chat = require('./chat')
var commands = require('./commands.js')
var statistics = require('./statistics')

module.exports = (io) => {
	io.on('connection', function(socket){
	  socket.on('chat message', chat(io));

	  socket.on('create statistics', statistics(io))

	  socket.on('create command', commands(io))
	});
}