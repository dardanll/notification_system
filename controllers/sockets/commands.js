 var Account = require('../../models/account');
 var Activity = require('../../models/activity');
 var ObjectId = require('mongodb').ObjectID;

module.exports = (io) => {
	return function(data){
    var activity = new Activity({type: data.type, message: data.msg});
    Account.findOne({ _id: ObjectId(data.user_id)}, (err, author) => {
      Account.findOne({ _id: ObjectId(data.receiver)}, (err, receiver) => {
        
        activity.receiver = receiver;
        activity.author = author;

        activity.save(err=> {
          receiver.activities.push(activity)
          receiver.save()
          author.activities.push(activity)
          author.save()
        });
      })
    })

    io.emit('create command');
  }
}