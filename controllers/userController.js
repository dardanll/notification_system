var Account = require('../models/account');
var Activity = require('../models/activity');
var ObjectId = require('mongodb').ObjectID;

module.exports.actions = (req, res) => {
	Account.findOne({_id: req.user._id}).populate({path: 'activities', populate: { path: 'author', model: 'Account'}}).exec((err, account) => {

		commands = find_commands(account.activities);
		upate_commands(req.user);

		res.render('users/actions', { activities: account.activities });
	});
}

module.exports.statistics =  (req, res) => {
	res.render('users/statistics');
}

// find activities which are command and not executed
function find_commands(activities){
	return activities.filter( activity => {
		return activity.type == 'command' && activity.executed == false; 
	})
}

//update executed field of activities to true
function upate_commands(user) {
	Activity.update(
	  {'receiver': ObjectId(user._id),  "executed": false },
	  {$set: {executed: true}},
	  {"multi": true} //for multiple documents
	).exec()
}