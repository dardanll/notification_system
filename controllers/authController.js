var Account = require('../models/account');
var passport = require('passport');

module.exports.login_get = (req, res) => {
  if (req.user) {
    if (req.user.admin)
      res.redirect('/admin/actions')
    else
      res.redirect('/user/actions')
  } else {
    res.render('auth/login', {
        user: req.user
    });
  }
}

module.exports.login_post = (req, res, next) => {
  Account.update_last_login(req.user._id, (err, account) => {
    if (err)
      next(err);
  })

  if (req.user.admin)
    res.redirect('/');
  else
    res.redirect('/');
}

module.exports.logout = (req, res) => {
  req.logout();
  res.redirect('/');
}

module.exports.register_get = (req, res) => {
  res.render('auth/register', {});
}

module.exports.register_post = (req, res) => {
  Account.register(new Account({
    username: req.body.username,
    admin: false
  }), req.body.password, function(err, account) {
      if (err) {
        return res.render('auth/register', {
          account: account
        });
      }

      passport.authenticate('local')(req, res, () => {
        res.redirect('/');
      });
  });
}