var Activity = require('../models/activity');
var Account = require('../models/account');

module.exports.actions = (req, res) => {
	Activity.find({}).populate('author').exec((err, activities) => {
		res.render('admin/actions', { activities: activities });
	})
}

module.exports.commands = (req, res) => {
	Account.find({admin: false}).exec((err, users) => {
		res.render('admin/commands', { users: users });
	})
}

module.exports.users = (req, res) => {
	Account.find({admin: false}).populate('activities').exec((err, users) => {
	  res.render('admin/users', {users: users});	
	})
}