function requireUser(req, res, next) {
  if (req.user && !req.user.admin){
    next();
  } else {
    var err = new Error("You must be a user to access this page");
    err.status = 401;
    next(err);
  }
}

function requireAdmin(req, res, next) {
  if (req.user && req.user.admin){
    next();
  } else {
    var err = new Error("You must be an admin to access this page");
    err.status = 401;
    next(err);
  }
}

function requireLogin(req, res, next) {
  if (req.user){
    next();
  } else {
    var err = new Error("You must be login to access this page");
    err.status = 401;
    next(err);
  }
}

module.exports.requireAdmin = requireAdmin
module.exports.requireUser = requireUser
module.exports.requireLogin = requireLogin