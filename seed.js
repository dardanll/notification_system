var mongoose = require('mongoose');
var Account = require('./models/account');

mongoose.connect('mongodb://localhost/notification_system', 
                { useMongoClient: true} );

Account.register(new Account({ username : 'admin', admin: true }), 'admin', function(err, account) {
  if (err){
  	console.log(err.message)
  	process.exit();
  }
  console.log('User created')
  process.exit();
});
