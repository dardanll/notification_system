var express = require('express');
var userController = require('../controllers/userController');
var router = express.Router();

router.use('/actions', userController.actions)
router.use('/statistics', userController.statistics)

module.exports = router;