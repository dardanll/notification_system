var express = require('express');
var authController = require('../controllers/authController');
var passport = require('passport');
var router = express.Router();

router.get('/', (req, res) => {
  if(req.user){
    if(req.user.admin)
      res.redirect('/admin/actions')
    else
       res.redirect('/user/actions')
  } else {
    res.render('auth/login');
  }
});

router.get('/login', authController.login_get);
router.post('/login', passport.authenticate('local', { failureRedirect: '/login'}), authController.login_post);
router.get('/logout', authController.logout);
router.get('/register', authController.register_get);
router.post('/register', authController.register_post);

module.exports = router;