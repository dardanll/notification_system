var express = require('express');
var router = express.Router();
var adminController = require('../controllers/adminController')

router.get('/actions', adminController.actions)
router.get('/commands', adminController.commands)
router.get('/users', adminController.users)

module.exports = router