const express = require('express');
var mid = require('../middleware');
var router = express.Router()

router.use('/', require('./auth'));
router.use('/user', mid.requireUser, require('./user'));
router.use('/admin', mid.requireAdmin, require('./admin'));
router.use('/chat', mid.requireLogin, require('./chat'));

module.exports = router
