var express = require('express');
var chatController = require('../controllers/chatController')
var router = express.Router();

router.get('/', chatController.index);

module.exports = router;