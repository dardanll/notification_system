var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose');

var Account = new Schema({
    username: String,
        password: String,
        admin: {
        	type: Boolean,
        	defualt: false
        },
        last_login_date: {
            type: Date,
            default: Date.now
        },
        activities: [{
        	type: Schema.Types.ObjectId, 
        	ref: 'Activity' 
        }]
    },
    { 
        timestamps: true
    });

Account.statics.update_last_login = function (id, callback) {
   return this.findByIdAndUpdate(id, { 'last_login_date' : Date.now() }, { new : true }, callback);
};

Account.plugin(passportLocalMongoose);

module.exports = mongoose.model('Account', Account);
