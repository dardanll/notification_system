var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ActivitySchema = new Schema({
    type: String,
    message: String,
    executed: {
      type: Boolean,
      default: false
    },
    author: {
    	type: Schema.Types.ObjectId, 
    	ref: 'Account' 
    },
    receiver: {
    	type: Schema.Types.ObjectId, 
    	ref: 'Account'
    }
	}, 
	{ 
		timestamps: true
	});


module.exports = mongoose.model('Activity', ActivitySchema)